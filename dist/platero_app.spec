# -*- mode: python -*-

block_cipher = pyi_crypto.PyiBlockCipher(key='Plateroespequeno')


a = Analysis(['../platero/platero_app.py'],
             pathex=['/Users/serrano/Work/CSF/Projects/GMI - Belkhadir - Protein Interaction Screen/dist'],
             binaries=None,
             datas=None,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
			 
a.datas += [ 
	('assets/gui/icon.gif', '../platero/assets/gui/icon.gif', 'DATA' )
]
			 
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='platero_app',
          debug=False,
          strip=False,
          upx=True,
          console=True,
		  icon='../platero/assets/gui/icon.icns')
		  
app = BUNDLE(exe,
             name='Platero.app',
			 version='0.1.4',
             icon='../platero/assets/gui/icon.icns',
		     author="Pedro Serrano Drozdowskyj",
		     author_email="pedro.serrano@csf.ac.at",
		     url="http://www.csf.ac.at/biocomp",
             bundle_identifier=None)
