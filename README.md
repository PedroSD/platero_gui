# README #

A little help explanation on how to get the code and run it.

### What is this repository for? ###

This project contains apps and scripts related to the CSI protein interaction screen performed by Elwira Smakowska from the Belkhadir group at the GMI. The main functionality relates to generating pipetting templates and automatically processing the results of the plate reader.


### How do set it up? ###

*NOTE:* instructions tested on Xubuntu (Ubuntu 14.04.4 LTS)

```
#!bash

# Go to your folder of choice
cd foo

# Clone the repository (this will save it into 'src' subfolder)
git clone https://bitbucket.org/PedroSD/platero_gui.git src 

# Create a virtual environment, using python 3.4 and activate it
virtualenv -p python3.4 venv
source venv/bin/activate

# Upgrade PIP and install requirements (this WILL take a few minutes!)
pip install --upgrade pip
pip install -r src/requirements.txt

# Run the platero app (from the containing folder!)
cd src/platero
./platero_app

# You can deactivate the virtual environment with
deactivate

```

### How to use it ###

* Start the platero_app
* Select a plates folder (e.g. /platero/data/plates/2015_08)
* Select a CSI list file (e.g. /platero/data/CSI_Screen.xlsx)
* Select an output folder for the results
* Click "Process plates"

![platero_app.png](https://bitbucket.org/repo/pd4A5E/images/3976315693-platero_app.png)


### Folder structure ###

Here's a quick overview of the repository's structure


```

	.
	├── data
	│   ├── CSI_Screen.xlsx				: The "database" in Elwira's eyes (an excel file she can update)
	│   ├── plates					: Results of the plate reader, organized by month and comparission (e.g. batch 1vs3)
	│   │   ├── 2015_07
	│   │   ├── 2015_08
	│   │   └── 2015_10
	│   ├── storage					: Storage templates
	│   │   ├── batch_01_bait_1.xlsx
	│   │   ├── batch_01_bait_2.xlsx
	│   │   ├── batch_01_bait_3.xlsx
	│   │   ├── batch_01_bait_4.xlsx
	│   │   ├── batch_01_prey.xlsx
	│   │   ├── batch_03_bait_1.xlsx
	│   │   ...
	│   │   	
	│   └── templates				: Plate reader/pipetting templates
	│       ├── plate_00022_b03_p03_template.xlsx
	│       ├── plate_00023_b03_p03_template.xlsx
	│       ├── plate_00024_b03_p03_template.xlsx
	│       └── ...
	├── dist
	│   └── platero_app.spec			: Configuration for distribution as packed app (PyInstaller)
	├── docs
	│   ├── first_batch				: The 1st batch was a special case, as it was done by Elwira manually
	│   │   ├── 24.06.2015 info_template.xlsx
	│   │   ├── Batch 10 SRF8 SRF9.xls
	│   │   ├── batch 11 At2g BIR1.xlsx
	│   │   └── ...
	│   ├── papers					: Some useful reads
	│   └── ProjectNotes.key			: Random mental notes after talking to Elwira about the project
	├── platero					: Main folder for the actual code
	│   ├── assets					: Static assets (e.g. icon for the app)
	│   │   └── gui
	│   ├── config.ini				: An file to save GUI selected options (in "development" version)
	│   ├── db					: Contains the SQLite DB file
	│   ├── platero					: Main application package, contains all the developed modules
	│   │   ├── assets				
	│   │   ├── model				: ORM classes
	│   │   ├── parsing				: File/XLS parsing classes
	│   │   ├── __init__.py
	│   │   ├── commands.py
	│   │   ├── config.py
	│   │   ├── platero.py
	│   │   ├── templates.py
	│   │   ├── utils.py
	│   │   └── wellplate.py
	│   ├── requirements.txt			: A file that describes the dependencies of the python project (can be used to setup the virtual environment
	│   ├── platero_app.py				: GUI application
	│   ├── init_db.py				: This and other python scripts are/were used for other tasks
	│   ├── process_plates.py			  (like generating templates), but might be outdated!
	│   ├── process_results.py
	│   ├── screen_templates.py
	│   ├── storage_templates.py
	│   └── templates_batch_1.py
	└── venv					: Virtual environment folder (you shouldn't need to touch here)

```