# Data #

This folder contains data files for the CIS screen project

## Files ##

 - README.txt : this file
 - CSI_Screen.xlsx : overview file with information about the screen

## Subfolders ##

 - plates : organised by screening month subfolders, it will contain all data
files for each plate:
  * plate_XXXXX_template : the plate template, for pipetting and reading in the results (includes the timesheet field)
  * plate_XXXXX_results : coming directly from the plate reader
  * plate_XXXXX_scan : image of the scanned plate

 - storage : includes all the storage templates for the different batches of proteins

 - templates : generated templates that have not been prepared in the lab yet
